CREATE DATABASE battlefield;
USE battlefield;
CREATE TABLE `reports` (
	`id` mediumint NOT NULL, AUTO_INCREMENT,
	`ammunition` SMALLINT unsigned NOT NULL,
	`soldiers` SMALLINT unsigned NOT NULL,
	`duration` double(6,1) unsigned NOT NULL,
	`critique` tinytext NOT NULL,
	`posted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (id)
	); 